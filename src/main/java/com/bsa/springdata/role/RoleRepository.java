package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Transactional
    @Modifying
    @Query(value =
            "with deletedRoles as " +
                "( " +
                    "delete  " +
                    "from roles as r " +
                    "where not exists " +
                    "( " +
                        "select user_id " +
                        "from user2role " +
                        "where role_id = r.id and user_id is not null " +
                    ") " +
                    "and r.code = :roleCode returning r.id" +
                ") " +
            "delete  " +
            "from user2role as ur  " +
            "where ur.role_id in (select id from deletedRoles)"
            , nativeQuery = true)
    void deleteByCode(@Param("roleCode") String roleCode);

}
