package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(nativeQuery = true,
            value =
            "select p.id, p.name, p.description " +
            "from projects as p " +
                     "inner join teams as tm " +
                                "on p.id = tm.project_id " +
                     "inner join technologies as tech " +
                                "on tech.name = :technology " +
                                    "and tm.technology_id = tech.id " +
            "order by ( " +
                         "select count(*) " +
                         "from users as u " +
                                  "inner join teams as tm2 " +
                                             "on tm.id = tm2.id " +
                                                 "and u.team_id = tm2.id " +
                     ") " +
            "limit 5")
    List<Project> findTop5ByTechnology(String technology);

    @Query(nativeQuery = true,
            value = "select p.id, p.name, p.description " +
                    "from projects as p " +
                             "inner join teams as tm " +
                                        "on p.id = tm.project_id " +
                             "inner join users as u " +
                                        "on tm.id = u.team_id " +
                    "group by p.id " +
                    "order by count(tm.id) desc, count(u.id) desc, p.name desc " +
                    "limit 1 ")
    Optional<Project> findTheBiggest();

    @Query(nativeQuery = true,
            value = "select p.name, " +
                            "count(distinct tm.id) as teamsNumber, " +
                            "count(distinct u.id) as developersNumber, " +
                            "string_agg(distinct tech.name, ',' order by tech.name desc) as technologies " +
                    "from projects as p " +
                             "inner join teams as tm on p.id = tm.project_id " +
                             "inner join users as u on tm.id = u.team_id " +
                             "inner join technologies as tech on tm.technology_id = tech.id " +
                    "group by p.name")
            List<ProjectSummaryDto> getSummary();

    @Query(nativeQuery = true,
            value = "select count(distinct p.id) " +
                    "from projects as p " +
                             "inner join teams as tm on p.id = tm.project_id " +
                             "inner join users as u on tm.id = u.team_id " +
                             "inner join user2role as ur on u.id = ur.user_id " +
                             "inner join roles as r on ur.role_id = r.id " +
                    "where r.name = :role ")
    int getCountWithRole(String role);

}
