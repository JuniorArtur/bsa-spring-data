package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;

    private final TechnologyRepository technologyRepository;

    private final TeamRepository teamRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, TechnologyRepository technologyRepository,
                          TeamRepository teamRepository) {
        this.projectRepository = projectRepository;
        this.technologyRepository = technologyRepository;
        this.teamRepository = teamRepository;
    }

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        final var projects = projectRepository.findTop5ByTechnology(technology);
        return projects.isEmpty()
                ? Collections.emptyList()
                : projects
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return projectRepository.findTheBiggest().map(ProjectDto::fromEntity);
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto request) {
        final var technology = new Technology();
        technology.setName(request.getTech());
        technology.setDescription(request.getTechDescription());
        technology.setLink(request.getTechLink());

        final var team = new Team();
        team.setName(request.getTeamName());
        team.setArea(request.getTeamArea());
        team.setRoom(request.getTeamRoom());
        team.setTechnology(technology);

        final var project = new Project();
        project.setName(request.getProjectName());
        project.setDescription(request.getProjectDescription());

        teamRepository.save(team);
        technologyRepository.save(technology);
        return projectRepository.save(project).getId();
    }

}
