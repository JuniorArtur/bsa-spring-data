package com.bsa.springdata.project;

import com.bsa.springdata.team.Team;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "name", columnDefinition="TEXT")
    private String name;

    @Column(name = "description", columnDefinition="TEXT")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private List<Team> teams;

}
