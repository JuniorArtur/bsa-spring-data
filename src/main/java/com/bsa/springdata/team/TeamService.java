package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    private final TeamRepository teamRepository;

    private final TechnologyRepository technologyRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository, TechnologyRepository technologyRepository) {
        this.teamRepository = teamRepository;
        this.technologyRepository = technologyRepository;
    }

    public void updateTechnology(int devsLimit, String oldTechnologyName, String newTechnologyName) {
        final var teams = teamRepository.findByTechnologyName(oldTechnologyName);
        for (final var team : teams) {
            if (team.getUsers().size() < devsLimit) {
                technologyRepository.updateTechnology(oldTechnologyName, newTechnologyName);
            }
        }
    }

    public void normalizeName(String name) {
        teamRepository.normalizeName(name);
    }

}
