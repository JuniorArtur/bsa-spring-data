package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {

    Optional<Technology> findByName(String name);

    @Transactional
    @Modifying
    @Query(value =
            "update technologies " +
            "set name = :newName " +
            "where name = :oldName",
            nativeQuery = true)
    void updateTechnology(@Param("oldName") String oldName, @Param("newName") String newName);

}
