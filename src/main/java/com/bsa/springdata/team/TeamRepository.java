package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findByName(String teamName);

    List<Team> findByTechnologyName(String technologyName);

    Long countByTechnologyName(String technology);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = "update teams set name = (:oldName || '_' || (p.name) || '_' || (tech.name)) " +
                    "from projects as p, technologies as tech " +
                    "where teams.name = :oldName and p.id = teams.project_id and tech.id = teams.technology_id"
    )
    void normalizeName(@Param("oldName") String oldName);

}
