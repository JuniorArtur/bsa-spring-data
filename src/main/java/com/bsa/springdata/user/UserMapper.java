package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface UserMapper {

    static List<UserDto> mapUsersToUserDtos(List<User> users) {
        return users.isEmpty()
                ? Collections.emptyList()
                : users
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

}
