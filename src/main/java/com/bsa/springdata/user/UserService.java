package com.bsa.springdata.user;

import com.bsa.springdata.office.OfficeRepository;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private static final Sort SORT_BY_LAST_NAME_ASC = Sort.by(Sort.Direction.ASC, "lastName");

    private final UserRepository userRepository;

    private final OfficeRepository officeRepository;

    private final TeamRepository teamRepository;

    @Autowired
    public UserService(UserRepository userRepository, OfficeRepository officeRepository,
                       TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.officeRepository = officeRepository;
        this.teamRepository = teamRepository;
    }

    public Optional<UUID> safeCreateUser(CreateUserDto userDto) {
        var office = officeRepository.findById(userDto.getOfficeId());
        var team = teamRepository.findById((userDto.getTeamId()));

        return office.flatMap(o -> team.map(t -> {
            var user = User.fromDto(userDto, o, t);
            var result = userRepository.save(user);
            return result.getId();
        }));
    }

    public Optional<UUID> createUser(CreateUserDto userDto) {
        try {
            var office = officeRepository.getOne(userDto.getOfficeId());
            var team = teamRepository.getOne(userDto.getTeamId());

            var user = User.fromDto(userDto, office, team);
            var result = userRepository.save(user);
            return Optional.of(result.getId());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<UserDto> getUserById(UUID id) {
        return userRepository.findById(id).map(UserDto::fromEntity);
    }

    public List<UserDto> getUsers() {
        return UserMapper.mapUsersToUserDtos(userRepository.findAll());
    }

    public List<UserDto> findByLastName(String lastName, int page, int size) {
        final var pr = PageRequest.of(page, size, SORT_BY_LAST_NAME_ASC);
        final var users = userRepository.findByLastNameIgnoreCaseContaining(lastName, pr);
        return UserMapper.mapUsersToUserDtos(users);
    }

    public List<UserDto> findByCity(String city) {
        final var users = userRepository.findByOfficeCity(city, SORT_BY_LAST_NAME_ASC);
        return UserMapper.mapUsersToUserDtos(users);
    }

    public List<UserDto> findByExperience(int experience) {
        final var users = userRepository.findByExperienceIsGreaterThanEqual(experience,
                Sort.by(Sort.Direction.DESC, "experience")
        );
        return UserMapper.mapUsersToUserDtos(users);
    }

    public List<UserDto> findByRoomAndCity(String city, String room) {
        final var users = userRepository.findByOfficeCityAndTeamRoom(city, room, SORT_BY_LAST_NAME_ASC);
        return UserMapper.mapUsersToUserDtos(users);
    }

    public int deleteByExperience(int experience) {
        return userRepository.deleteByExperienceLessThan(experience);
    }

}
