package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameIgnoreCaseContaining(String lastName, Pageable p);

    List<User> findByOfficeCity(String city, Sort order);

    List<User> findByExperienceIsGreaterThanEqual(int experience, Sort order);

    List<User> findByOfficeCityAndTeamRoom(String city, String room, Sort order);

    @Modifying
    @Query(value = "delete from User u where u.experience < :experience")
    int deleteByExperienceLessThan(@Param("experience") int experience);

}
