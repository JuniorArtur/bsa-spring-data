package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(value =
            "select distinct of.id, of.city, of.address " +
            "from offices as of " +
                "inner join users as u on of.id = u.office_id " +
                "inner join teams as tm on u.team_id = tm.id " +
                "inner join technologies as tech on tm.technology_id = tech.id " +
            "where tech.name = :technology",
            nativeQuery = true)
    List<Office> findOfficeByTechnology(@Param("technology") String technology);

    @Query(value =
            "with updated as  " +
                     "( " +
                         "update offices  " +
                             "set address = :newAddress  " +
                             "where address = :oldAddress  " +
                             "returning id, city, address  " +
                     ") " +
            "select up.id, up.city, up.address  " +
            "from updated as up  " +
            "where exists  " +
                     "(  " +
                    "select u.office_id  " +
                    "from users as u  " +
                    "where u.office_id = up.id  " +
                    ")",
            nativeQuery = true)
    Optional<Office> updateAddress(@Param("oldAddress") String oldAddress, @Param("newAddress") String newAddress);

}
